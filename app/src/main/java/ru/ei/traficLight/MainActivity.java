package ru.ei.traficLight;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.myapplication.R;

import static com.myapplication.R.id.background;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void onClick(View view) {
        ConstraintLayout bg = (ConstraintLayout) findViewById(R.id.background);
        switch (view.getId()) {
            case R.id.button_red:
                // кнопка RED
                bg.setBackgroundColor(Color.RED);
                break;
            case R.id.button_yellow:
                // кнопка BLUE
                bg.setBackgroundColor(Color.YELLOW);
                break;
            case R.id.button_green:

                bg.setBackgroundColor(Color.GREEN);
                break;
        }
    }
}
